#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <time.h>
#include <chrono>
#include "device_functions.h"

#ifdef __INTELLISENSE___
// in here put whatever is your favorite flavor of intellisense workarounds
#endif
using namespace std;
#define N 100
#define THREADS_PER_BLOCK 1024

__device__
void swap(float* a, float* b) {
    float tmp = *a;
    *a = *b;
    *b = tmp;
}

__global__ void integration(float *d_matX, float* d_matY, unsigned long long mat_size, float *suma)
{
    int index = threadIdx.x + blockIdx.x * blockDim.x;
    float dx = 0;
    if (index < mat_size - 1)
    {
        atomicAdd(suma, (d_matX[index + 1] - d_matX[index]) * 0.5 * (d_matY[index + 1] + d_matY[index]));
    }


}

__global__ void addKernel(float* d_matX, float* d_matY, unsigned long long mat_size)
{
    /*if (index < mat_size - 1)
    {
        if (jindex < mat_size - index - 1)
        {
            if (d_matX[jindex] > d_matX[jindex + 1])
            {
                float temp = d_matX[jindex];
                d_matX[jindex] = d_matX[jindex+1];
                d_matX[jindex + 1] = temp;

                float temp2 = d_matY[jindex];
                d_matY[jindex] = d_matY[jindex + 1];
                d_matY[jindex + 1] = temp;
                //swap_gpu(d_matX[jindex], d_matX[jindex + 1]);
                //swap_gpu(d_matY[jindex], d_matY[jindex + 1]);
            }
        }
    }*/
    int tIdx = threadIdx.x + blockIdx.x * blockDim.x;
    for (unsigned long long i = 0; i < mat_size; i++)
    {
        int offset = i % 2;
        int leftIndex = 2 * tIdx + offset;
        int rightIndex = leftIndex + 1;

        if (rightIndex < mat_size)
        {
            if (d_matX[leftIndex] > d_matX[rightIndex])
            {
                float temp = d_matX[leftIndex];
                d_matX[leftIndex] = d_matX[rightIndex];
                d_matX[rightIndex] = temp;

                float temp2 = d_matY[leftIndex];
                d_matY[leftIndex] = d_matY[rightIndex];
                d_matY[rightIndex] = temp2;
            }
        }
        __syncthreads();
    }
}




void swap(float xp, float yp)
{
    float temp = xp;
    xp = yp;
    yp = temp;
}

void bubbleSort(float arr[], float arr2[], unsigned long long n)
{
    unsigned long long i, j;
    for (i = 0; i < n - 1; i++)

        // Last i elements are already in place 
        for (j = 0; j < n - i - 1; j++)
            if (arr[j] > arr[j + 1])
            {
                swap(arr[j], arr[j + 1]);
                swap(arr2[j], arr2[j + 1]);
            }
}


void trapezoid_integration(float arrX[], float arrY[], unsigned long long size, float s)
{

    float dx = 0;
    for (unsigned long long i = 0; i < size - 1; i++)
    {
        dx = arrX[i + 1] - arrX[i];
        s += 0.5 * (arrY[i] + arrY[i + 1]) * dx;
        // cout << s << endl;
         //cout <<  arrY[i] << endl;
    }

    cout << "suma na cpu" << s << endl;


}


void generator(float matrix_x[], float matrix_y[], unsigned long long size)
{
    srand(time(NULL));


    for (unsigned long long j = 0; j < size; j++)
    {
        matrix_y[j] = float(rand()) / float((RAND_MAX)) * 100000;
    }


    for (unsigned long long i = 0; i < size; i++)
    {
        bool check;
        float rand_liczba;
        do
        {
            rand_liczba = float(float(rand())) / float((RAND_MAX)) * 100000;
            check = true;
            for (unsigned long long j = 0; j < i; j++)
            {
                if (rand_liczba == matrix_x[j])
                {
                    check = false;
                    break;
                }
            }
        } while (!check);
        matrix_x[i] = rand_liczba;
    }

}

int main()
{
    unsigned long long size;
    unsigned long long* d_size;
    float s = 0;
    float* d_matX;
    float* d_matY;
    cout << "ile punktow chcesz wygenerowac: ";
    cin >> size;
    cout << endl;
    int size_of_size = sizeof(unsigned long long);

    int size_cuda = size * sizeof(float);

    float *suma = 0;
    float* d_suma;
    float* matrix_X = new float[size];
    float* matrix_Y = new float[size];

    cout << "zaczynam generowanie: " << endl;
    generator(matrix_X, matrix_Y, size);
    cout << "zaczynam sortowanie i calkowanie na cpu: " << endl;

    auto start = std::chrono::system_clock::now();
    bubbleSort(matrix_X, matrix_Y, size);
    trapezoid_integration(matrix_X, matrix_Y, size, s);
    auto stop = std::chrono::system_clock::now();


    std::chrono::duration<double> diff = stop - start;
    cout << "sortowanie na cpu zajelo: "<<diff.count()<<endl;



    unsigned long long no_of_blocks = ceil(size / THREADS_PER_BLOCK) + 1;

    auto start_gpu = std::chrono::system_clock::now();

    cudaMalloc((void**)&d_matX, size_cuda);
    cudaMalloc((void**)&d_matY, size_cuda);
    cudaMalloc((void**)&d_suma, sizeof(float));

    cudaMemcpy(d_matX, matrix_X, size_cuda, cudaMemcpyHostToDevice);
    cudaMemcpy(d_matY, matrix_Y, size_cuda, cudaMemcpyHostToDevice);
    cudaMemcpy(d_suma, suma, sizeof(float), cudaMemcpyHostToDevice);
    
    addKernel << <no_of_blocks, THREADS_PER_BLOCK >> > (d_matX, d_matY, size);
    integration << < no_of_blocks, THREADS_PER_BLOCK >> > (d_matX, d_matY, size, d_suma);

    cudaMemcpy(matrix_X, d_matX, size_cuda, cudaMemcpyDeviceToHost);
    cudaMemcpy(matrix_Y, d_matY, size_cuda, cudaMemcpyDeviceToHost);
    cudaMemcpy(suma, d_suma, sizeof(float), cudaMemcpyDeviceToHost);

    cudaFree(d_matX);
    cudaFree(d_matY);
    cudaFree(d_suma);

    auto stop_gpu = std::chrono::system_clock::now();

    std::chrono::duration<double> diff_gpu = stop_gpu - start_gpu;
    cout << "sortowanie na gpu zajelo: " <<diff_gpu.count()<< endl;
    cout << "suma na gpu" <<*suma<< endl;



    cout << "calka wynosi: " << s << endl;

    delete[] matrix_X;
    delete[] matrix_Y;

    return 0;
}
